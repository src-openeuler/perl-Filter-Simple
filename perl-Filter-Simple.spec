%global src_package Filter-Simple-0.94
Name:           perl-Filter-Simple
Epoch:          1
Version:        0.94
Release:        3
Summary:        Simplified Perl source filtering
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Filter-Simple
Source0:        https://cpan.metacpan.org/authors/id/S/SM/SMUELLER/%{src_package}.tar.gz
BuildArch:      noarch

BuildRequires:  make perl-generators perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict) perl(Carp)
BuildRequires:  perl(Filter::Util::Call)
BuildRequires:  perl(Text::Balanced) >= 1.97
BuildRequires:  perl(vars) perl(warnings) perl(Exporter) perl(parent)
Requires:       perl(Text::Balanced) >= 1.97
Requires:       perl(warnings)

%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Text::Balanced\\)$

%description
The Filter::Simple Perl module provides a simplified interface to
Filter::Util::Call; one that is sufficient for most common cases.

%package help
BuildArch: noarch
Summary: Document for the Filter::Simple

%description help
Document for the Filter::Simple Perl module

%prep
%autosetup -n %{src_package} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{perl_vendorlib}/*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1:0.94-3
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Aug 1 2024 yanglongkang <yanglongkang@h-partners.com> - 1:0.94-2
- rebuild for next release

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:0.94-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary files

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.95-418
- Package init
